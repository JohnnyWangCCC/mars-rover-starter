package com.afs.tdd;

public interface MarsRoverCommand {
    void execute(Location location);
}
