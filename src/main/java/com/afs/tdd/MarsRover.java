package com.afs.tdd;

import java.util.List;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return this.location;
    }

    public void executeCommand(MarsRoverCommand command) {
        command.execute(location);
    }

    public void executeBatchCommands(List<MarsRoverCommand> commands) {
        commands.forEach(this::executeCommand);
    }
}
