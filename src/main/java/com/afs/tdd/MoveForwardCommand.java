package com.afs.tdd;

public class MoveForwardCommand implements MarsRoverCommand{
    public void execute(Location location) {
        switch (location.getDirection()){
            case EAST:
                location.setLocationX(location.getLocationX() + 1);
                break;
            case North:
                location.setLocationY(location.getLocationY() + 1);
                break;
            case South:
                location.setLocationY(location.getLocationY() - 1);
                break;
            case West:
                location.setLocationX(location.getLocationX() - 1);
                break;
        }
    }
}
