package com.afs.tdd;

public class TurnRightCommand implements MarsRoverCommand {
    public void execute(Location location) {
        switch (location.getDirection()){
            case North:
                location.setDirection(Direction.EAST);
                break;
            case EAST:
                location.setDirection(Direction.South);
                break;
            case South:
                location.setDirection(Direction.West);
                break;
            case West:
                location.setDirection(Direction.North);
                break;
        }
    }
}
