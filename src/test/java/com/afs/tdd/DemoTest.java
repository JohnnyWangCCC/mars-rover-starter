package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class DemoTest {
    @Test
    void should_only_plus_location_y_1_when_executeCommand_given_location_and_command_move() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new MoveForwardCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(0,changedLocation.getLocationX());
        Assertions.assertEquals(1,changedLocation.getLocationY());

    }

    @Test
    void should_only_plus_location_x_1_when_executeCommand_given_location_and_command_move() {
        //given
        Location location = new Location(0,0,Direction.EAST);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new MoveForwardCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(1,changedLocation.getLocationX());
        Assertions.assertEquals(0,changedLocation.getLocationY());

    }

    @Test
    void should_only_sub_location_y_1_when_executeCommand_given_location_and_command_move() {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new MoveForwardCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(0,changedLocation.getLocationX());
        Assertions.assertEquals(-1,changedLocation.getLocationY());

    }

    @Test
    void should_only_sub_location_x_1_when_executeCommand_given_location_and_command_move() {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new MoveForwardCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(-1,changedLocation.getLocationX());
        Assertions.assertEquals(0,changedLocation.getLocationY());

    }

    @Test
    void should_only_change_direction_west_when_executeCommand_given_direction_north_and_command_turn_left() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnLeftCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.West,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_south_when_executeCommand_given_direction_west_and_command_turn_left() {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnLeftCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.South,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_east_when_executeCommand_given_direction_south_and_command_turn_left() {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnLeftCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.EAST,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_north_when_executeCommand_given_direction_east_and_command_turn_left() {
        //given
        Location location = new Location(0,0,Direction.EAST);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnLeftCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.North,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_east_when_executeCommand_given_direction_north_and_command_turn_right() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnRightCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.EAST,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_south_when_executeCommand_given_direction_east_and_command_turn_right() {
        //given
        Location location = new Location(0,0,Direction.EAST);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnRightCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.South,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_west_when_executeCommand_given_direction_south_and_command_turn_right() {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnRightCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.West,changedLocation.getDirection());

    }

    @Test
    void should_only_change_direction_north_when_executeCommand_given_direction_west_and_command_turn_right() {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(new TurnRightCommand());

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.North,changedLocation.getDirection());

    }

    @Test
    void should_change_direction_north_and_sub_x_1_and_plus_y_1_when_executeBatchCommands_given_direction_north_and_command_with_move_turn_left_move_turn_right() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);
        ArrayList<MarsRoverCommand> commands = new ArrayList<>();
        commands.add(new MoveForwardCommand());
        commands.add(new TurnLeftCommand());
        commands.add(new MoveForwardCommand());
        commands.add(new TurnRightCommand());
        //when
        marsRover.executeBatchCommands(commands);

        //then
        Location changedLocation = marsRover.getLocation();
        Assertions.assertEquals(Direction.North,changedLocation.getDirection());
        Assertions.assertEquals(-1,changedLocation.getLocationX());
        Assertions.assertEquals(1,changedLocation.getLocationY());
    }
}
