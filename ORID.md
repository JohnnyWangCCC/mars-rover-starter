### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today's lesson covered Unit Test, TDD, and how to properly submit code in the TDD process. In the morning, we carried out CodeView according to the practice, and referred to the code of colleagues in CodeView, and also found their own shortcomings.In the context of software development, unit testing is a technique for testing individual units or components of a system in isolation. It helps ensure that each unit functions correctly on its own before integrating them into the larger system.While all the scenes were informative and impactful, one scene that particularly stood out was the demonstration of detecting and fixing bugs through effective unit tests and reviewing code. It highlighted the significance of these practices in ensuring software reliability and minimizing errors.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Devoted.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	The most meaningful part of today's session was in giving us guidance on a proper software development process, from Tasking to Context Map, and then from writing well-developed tests and following them to implement the business. The whole process provided a very good coding experience and also ensured that the writing of our business would not lead to errors in other businesses as a safeguard for the business.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Today's lesson can be practically applied to future business work, which is the actual process of the project of the team I am interning with. I will incorporate TDD ideas into my future project requirements and follow the correct process for new requirements.